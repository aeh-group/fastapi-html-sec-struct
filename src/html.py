from typing import Optional
from fastapi import FastAPI, Request, Form
from fastapi.templating import Jinja2Templates
from starlette.responses import FileResponse

from src.model import spell_number
from src.model import structinfo


app = FastAPI(
    title="sec-struct",
    description="Currently just a mock-up with auto docs for the API. Try <url>/form ",
    version="0.1.0",
)
templates = Jinja2Templates(directory='templates/')


def save_to_text(content, filename):
    filepath = 'tmp/{}.txt'.format(filename)
    with open(filepath, 'w') as f:
        f.write(content)
    return filepath


@app.get('/')
def read_form():
    return 'alive = true. For interactive-form try http://localhost:8000/form  (or just append /form to url)'

# https://github.com/tiangolo/fastapi/issues/854#issuecomment-573965912
@app.post("/my-form-endpoint")
def my_endpoint(
    my_field_with_default: str = Form("some value"),
    my_optional_field: Optional[int] = Form(None),
    my_required_field: str = Form(...),
) -> dict:
    return {
        "my_field_with_default": my_field_with_default,
        "my_optional_field": my_optional_field,
        "my_required_field": my_required_field,
    }

@app.get('/form')
def form_post(request: Request):
    result = 'Type sequence'
    return templates.TemplateResponse('form.html', context={'request': request, 'result': result})


@app.post('/form')
def form_post(request: Request, seq: str = Form(...)):
    result = structinfo(seq)
    return templates.TemplateResponse('form.html', context={'request': request, 'result': result})


@app.get('/checkbox')
def form_post(request: Request):
    result = 'Type a sequence'
    return templates.TemplateResponse('checkbox.html', context={'request': request, 'result': result})


@app.post('/checkbox')
def form_post(request: Request, num: int = Form(...), multiply_by_2: bool = Form(False)):
    result = spell_number(num, multiply_by_2)
    return templates.TemplateResponse('checkbox.html', context={'request': request, 'result': result, 'num': num})

@app.get('/download')
def form_post(request: Request):
    result = 'Type a seq'
    return templates.TemplateResponse('download.html', context={'request': request, 'result': result})


@app.post('/download')
def form_post(request: Request, num: int = Form(...), multiply_by_2: bool = Form(False), action: str = Form(...)):
    if action == 'convert':
        result = spell_number(num, multiply_by_2)
        return templates.TemplateResponse('download.html', context={'request': request, 'result': result, 'num': num})
    elif action == 'download':
        # Requires aiofiles
        result = spell_number(num, multiply_by_2)
        filepath = save_to_text(result, num)
        return FileResponse(filepath, media_type='application/octet-stream', filename='{}.txt'.format(num))
