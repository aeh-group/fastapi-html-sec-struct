import RNA
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from pydantic import BaseModel 
import json

def structinfo(seq: str):
#    seq = 'CGGAGGAACUACUGUCUUCACGCGCGUGUCGUGCAGCCUCCG'
#    seq = 'GGUGAAGGCUGCCGAAGCCA'
# create new fold_compound object
    fc = RNA.fold_compound(seq)
 
# compute minimum free energy (mfe) and corresponding structure
    (ss, mfe) = fc.mfe()

# supply mock values for 7 items in array a [ap,bulge,inner, sizeap,sizeblg,sizeinner, sm-hits]
    a = [0,0,0,0,0,0,0]
    if seq == 'GGUGCUGUUAUCCAUUUCAGAAUUGGGUGUCGACAUAGCACC':  # demo1
        a = [1,1,0,9,5,0,[[40,41,42,43,44],[-1]]]
    if seq == 'CGGAGGAACUACUGUCUUCACGCGCGUGUCGUGCAGCCUCCG':  # demo2
        a = [1,2,2,4,[5,1],[2,2],[[142,143,144,145],[21,22,61,62,63,64,65,69,70]]]
    if seq == 'GGACUAGCGGAGGCUAGUCC':                        # demo3
        a = [1,0,0,4,0,0,72]
    ruler = '123456789-123456789_123456789-123456789_123456789-123456789_123456789-123456789_1234567890'
    lenseq = len(seq)
    if lenseq > 90:
        lenseq = 90
 # TODO: here would be code that uses seq and/or ss(=dotbracket) to populate 7-items in array a[0 to 6]        
    result = json.dumps ({'len': ruler [ 0 : lenseq], 'seq': seq, 'dot': ss, 'apical': a[0], 'bulge': a[1], 'inner': a[2], \
        'apicalsize': a[3], 'bulgesize': a[4], 'innersize': a[5], 'SmallMolecules': a[6] }, sort_keys=False, indent=4)
    return (result)
#    return ' "nucleioide_seq": "' + nts + '"' + ss 


 
def spell_number(num: int, multiply_by_2: bool = False):
    d = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five',
         6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten',
         11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen',
         15: 'fifteen', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen',
         19: 'nineteen', 20: 'twenty',
         30: 'thirty', 40: 'forty', 50: 'fifty', 60: 'sixty',
         70: 'seventy', 80: 'eighty', 90: 'ninety'}
    k = 1000
    m = k * 1000
    b = m * 1000
    t = b * 1000

    assert (0 <= num)

    if multiply_by_2:
        num *= 2

    if num < 20:
        return d[num]

    if num < 100:
        if num % 10 == 0:
            return d[num]
        else:
            return d[num // 10 * 10] + '-' + d[num % 10]

    if num < k:
        if num % 100 == 0:
            return d[num // 100] + ' hundred'
        else:
            return d[num // 100] + ' hundred and ' + spell_number(num % 100)

    if num < m:
        if num % k == 0:
            return spell_number(num // k) + ' thousand'
        else:
            return spell_number(num // k) + ' thousand, ' + spell_number(num % k)

    if num < b:
        if (num % m) == 0:
            return spell_number(num // m) + ' million'
        else:
            return spell_number(num // m) + ' million, ' + spell_number(num % m)

    if num < t:
        if (num % b) == 0:
            return spell_number(num // b) + ' billion'
        else:
            return spell_number(num // b) + ' billion, ' + spell_number(num % b)

    if num % t == 0:
        return spell_number(num // t) + ' trillion'
    else:
        return spell_number(num // t) + ' trillion, ' + spell_number(num % t)
