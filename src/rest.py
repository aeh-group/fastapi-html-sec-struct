from typing import Optional
from fastapi import FastAPI, Form

@app.get("/rest")
def read_item(nts: str):
    result = structinfo(nts)
    return {"structinfo": result}

# https://github.com/tiangolo/fastapi/issues/854#issuecomment-573965912
@app.post("/my-form-endpoint")
def my_endpoint(
    my_field_with_default: str = Form("some value"),
    my_optional_field: Optional[int] = Form(None),
    my_required_field: str = Form(...),
) -> dict:
    return {
        "my_field_with_default": my_field_with_default,
        "my_optional_field": my_optional_field,
        "my_required_field": my_required_field,
    }