### fastapi-html-sec-Struct

Provides functionality described in [RBIND_vienna.pdf](RBIND_vienna.pdf)
Try it out at interactive web-site at `/form` that is, just append `/form` to the base url like http://localhost:8000/form (see screenshot below). Given one required input, a sequence, outputs secondary-structure info and hits to similar items in rhttp://bind.chem.duke.edu  
#### Example input:
Note: The line with dot-bracket notation is optional, and is calculated by RNAfold if not provided. 
```
CGGAGGAACUACUGUCUUCACGCGCGUGUCGUGCAGCCUCCG
((((((.....(((((..(((....)))..).))))))))))  
``` 
#### Example Proposed Output. 
Seven items are returned. First 3 are count of (A) apical, (B) bulge and (C) inner-loop structs. Next 3 are size(s), in units of nts,for each non-zero count. Last item is hits against rbind in form of each sm having same size of each A,B, and/or C struct. Might be something like this json (via interactive website and/or via a REST-API):
```json
{
    "seq": "CGGAGGAACUACUGUCUUCACGCGCGUGUCGUGCAGCCUCCG",
    "dot": "((((((.....(((((..(((....)))..).))))))))))",
    "apical": 1, 
    "bulge": 2, 
    "inner": 2, 
    "apicalsize": 4, 
    "bulgesize": [5, 1], 
    "innersize": [2, 2], 
    "SmallMolecules": [[142, 143, 144, 145], [21, 22, 61, 62, 63, 64, 65, 69, 70]]
}
```
Optionally also return table/array of selected info for each SM (SmallMolecule) hit, perhaps like:
```
Struct, nts, sm#, seq-frag, additional-selected-info
bulge1, 5  , 142, AACUA, selected info for SM-0142`
bulge1, 5  , 143, AACUA, selected info for SM-0143`
...
bulge2, 1, , 21, A, selected info for SM-0021
bulge2, 1, , 22, A, selected info for SM-0022

 and so on ...
```
TODO: deal with non-integer sm# like 22a ...

During development, a prototype may be available on Duke campus (or with VPN) at http://10.236.75.113:8000/form
(if the linux box in Hargrove lab is running). ![sec-struct-mockup.png](data/sec-struct-mockup.png)

## for developers
Adopted from simple [fastapi-html](https://github.com/eugeneyan/fastapi-html) demo example described here:
[https://eugeneyan.com/writing/how-to-set-up-html-app-with-fastapi-jinja-forms-templates/](https://eugeneyan.com/writing/how-to-set-up-html-app-with-fastapi-jinja-forms-templates/) 

To run locally... I don't think you need to `conda install` additional pkgs. TODO: Any issues using both conda and poetry in conda env?
```
$ conda create -n py3vienna viennarna -c bioconda
$ git clone <this repo>
$ cd fastapi-html-sec-structure
$ conda install fastapi poetry
$ poetry install fastapi uvicorn
$ poetry update fastapi uvicorn
$ echo "run in tmux: poetry run uvicorn src.rest:app --reload --port 8001" # for rest-api at port 8001
$ poetry run uvicorn src.html:app  --reload --host 0.0.0.0 #
$ echo "open http://localhost:8000/form "
```
TODO: gitlab ci/cd to spin up webserver with rest-api and /form interactive website.
